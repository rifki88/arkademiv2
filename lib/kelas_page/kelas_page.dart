import 'package:arkademi/auth_page/auth_page.dart';
import 'package:arkademi/auth_page/login_page.dart';
import 'package:arkademi/checkout_page/checkout_page.dart';
import 'package:arkademi/data/ulasan.dart' as ulasan;
import 'package:arkademi/kelas_page/deskripsi_page.dart';
import 'package:arkademi/kelas_page/kurikulum_page.dart';
import 'package:arkademi/kelas_page/ulasan_page.dart';
import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:arkademi/mulai_kelas_page/mulai_kelas_page.dart';
import 'package:arkademi/util/global_var.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:shared_preferences/shared_preferences.dart';


class KelasPage extends StatefulWidget {
  final bool isJoinKelas;
  KelasPage({
    this.isJoinKelas,
  });

  @override
  _KelasPageState createState() => _KelasPageState(
    isJoinKelas: isJoinKelas,
  );
}

class _KelasPageState extends State<KelasPage> {
  int tabIndex = 0;

  bool isJoinKelas = false;

  _KelasPageState({
    this.isJoinKelas,
  });

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 395,
                  color: Colors.blueGrey[700],
                ),
                Image.asset('assets/header.png'),
                Container(
                  margin: EdgeInsets.only(
                    top: 20,
                  ),
                  child: Stack(
                    children: <Widget>[
                      
                      Container(
                        margin: EdgeInsets.only(
                          top: 12
                        ),
                        width: MediaQuery.of(context).size.width,
                        child: Text("KELAS", style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                        ),
                      ),
                      IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
                      ),
                    ],
                  )
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 70
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.network("http://client.gardana.id/arkademi/banner-kelas.png", width: MediaQuery.of(context).size.width,),
                      Text("Mendirikan Startup & Cara Membagi Saham", style: TextStyle(
                        fontSize: 22,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.start,
                      ),
                      Container(height: 8,),
                      Text("Mentor : HILMAN", style: TextStyle(
                        fontSize: 12,
                        color: Colors.yellow,
                      ),
                      textAlign: TextAlign.start,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 10,
                        ),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                height: 22,
                                margin: EdgeInsets.only(
                                  right: 5,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Icon(Icons.people, size: 16,)
                                    ),
                                    Text("1.000 siswa", style: TextStyle(
                                      fontSize: 10,
                                      color: Colors.white,
                                    ),),
                                  ],
                                )
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 22,
                                
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Container(
                                        padding: EdgeInsets.only(
                                          left: 8,
                                          top: 5,
                                        ),
                                        child: Text(
                                          "4.8",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Text(
                                        "1.000 siswa", 
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ),
                            Expanded(
                              child: Container(
                                height: 22,
                                margin: EdgeInsets.only(
                                  left: 5,
                                ),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(10),
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10)
                                  ),
                                  color: Colors.blue
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(100),
                                          bottomLeft: Radius.circular(50),
                                          topLeft: Radius.circular(50)
                                        ),
                                        color: Colors.yellow
                                      ),
                                      height: 22,
                                      width: 40,
                                      child: Icon(Mdi.thumbUp, size: 16,)
                                    ),
                                    Expanded(
                                      child: Text(
                                        "5 ulasan", 
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 360,
                  ),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey[300],
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 0;
                          });
                        },
                        child: Container(
                          color: tabIndex == 0 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Deskripsi",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 0 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 0 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 1;
                          });
                        },
                        child: Container(
                          color: tabIndex == 1 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Kurikulum",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 1 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 1 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tabIndex = 2;
                          });
                        },
                        child: Container(
                          color: tabIndex == 2 ? Colors.brown : Colors.transparent,
                          child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                  top: 10,
                                ),
                                width: MediaQuery.of(context).size.width / 3,
                                height: 40,
                                child: Text(
                                  "Ulasan",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w300,
                                    color: tabIndex == 2 ? Colors.white : Colors.black,
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width / 3,
                                height: 3,
                                color: tabIndex == 2 ? Colors.orange : Colors.grey,
                              ),
                            ],
                          ),
                        ),
                      )
                      
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: 420,
                  ),
                  padding: EdgeInsets.only(
                    left: 15,
                    right: 15,
                    bottom: 30,
                  ),
                  width: MediaQuery.of(context).size.width,
                  child: content(),
                ),

                
              ],
            ),
          ),
          Positioned(
            bottom: 50,
            // left: MediaQuery.of(context).size.width / 2 - 150,
            left: 30,
            right: 30,
            child: InkWell(
              onTap: (){

              },
              child: joinWidget(95000, 500000),
            ),
          ),

          
        ],
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () => changeSelected(1),
        tooltip: 'Increment',
        backgroundColor: Colors.orange[700],
        child: Icon(Icons.person, size: 40,),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: _selectedIndex == 1 ? Colors.yellow : Colors.white,
        shape: FollowerNotchedShape(inverted: true),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            InkWell(
              onTap: () {
                changeSelected(0);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: _selectedIndex == 0 ? Colors.white : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.home),
                    Text("Home"),
                  ],
                ),
              ),
            ),
            
            InkWell(
              onTap: () {
                changeSelected(1);
              },
              child:Container(
                height: 50,
                width: 80,
                // color: Colors.red,
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Text("LOGIN", textAlign: TextAlign.center,),
              ),
            ),
            
            InkWell(
              onTap: () {
                changeSelected(2);
              },
              child: Container(
                decoration: BoxDecoration(
                  color: _selectedIndex == 2 ? Colors.yellow : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Icon(Icons.menu),
                    Text("Direktori"),
                  ],
                ),
              ),
            ),
            
            
          ],
        ),
        

      )
    );
  }

  int _selectedIndex = 0;
  
  void changeSelected(int index) {

  }

  void _mulaiKelas() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MulaiKelasPage()),
    );
  }

  void _bergabung() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => CheckoutPage()),
    );
  }

  Widget joinWidget(int hargaAsli, int hargaDiskon) {
    return InkWell(
      onTap: isJoinKelas == true ? _mulaiKelas : _bergabung,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: isJoinKelas == true ? Colors.green : Colors.amber[900],
          
        ),
        height: 50,
        width: MediaQuery.of(context).size.width,
        
        child: isJoinKelas == true ? Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Container(
                
                child: Text(
                  "MULAI KELAS",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ],
        ) : Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                
                child: Text(
                  "BERGABUNG",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Expanded(
              child: Container(
                padding: EdgeInsets.only(
                  right: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20)
                  ),
                  color: Colors.deepOrange[300],
                ),
                width: MediaQuery.of(context).size.width / 2,
                height: 50,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "Rp ${numberFormat(hargaDiskon)}" ,
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.right,
                    ),
                    Text(
                      "Rp ${numberFormat(hargaAsli)}" ,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                      textAlign: TextAlign.right,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget content() {
    if (tabIndex == 0) {
      return DeskripsiPage(
        title: "Salah Mendirikan Perusahaan dan Menyesal Selamanya",
        content: "Non consequat minim deserunt elit pariatur ea nostrud irure mollit laborum minim est sit. Do exercitation culpa laboris officia eu non sunt aliqua. Irure consectetur ullamco magna sit. Exercitation consequat reprehenderit minim minim sit do. Dolore ex qui in id commodo in et irure officia laborum est. Aliqua nisi esse mollit non magna sunt eu commodo ea ipsum nostrud reprehenderit nostrud magna. Tempor tempor eiusmod labore quis dolor labore laboris.",
      );
    }else if (tabIndex == 1) {
      return KurikulumPage();
    } else {
      return UlasanPage(
        ulasanList: ulasan.ulasanList,
      );
    }
  }

}
