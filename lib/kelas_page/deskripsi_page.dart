import 'package:flutter/material.dart';

class DeskripsiPage extends StatefulWidget {
  final String title;
  final String content;

  DeskripsiPage({
    this.title,
    this.content,
  });
  @override
  _DeskripsiPageState createState() => _DeskripsiPageState(
    title: title,
    content: content,
  );
}

class _DeskripsiPageState extends State<DeskripsiPage> {
  String title;
  String content;
  _DeskripsiPageState({
    this.title,
    this.content,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontSize: 30,
            ),
          ),
          Container(height: 10,),
          Text(
            content,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}