import 'package:arkademi/data/ulasan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:mdi/mdi.dart';

class UlasanPage extends StatefulWidget {
  final List<Ulasan> ulasanList;

  UlasanPage({
    this.ulasanList,
  });
  @override
  _UlasanPageState createState() => _UlasanPageState(
    ulasanList: ulasanList
  );
}

class _UlasanPageState extends State<UlasanPage> {
  List<Ulasan> ulasanList = [];
  List<Widget> ulasanWidgetList = [];

  _UlasanPageState({
    this.ulasanList
  });

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) => ulasanWidget());
  }

  void ulasanWidget() {
    ulasanWidgetList.clear();
    for(int i = 0; i < ulasanList.length;i++) {
      Ulasan ulasan = ulasanList[i];

      ulasanWidgetList.add(
        Container(
          margin: EdgeInsets.only(
            bottom: 20,
          ),
          child: InkWell(
            onTap: (){},
            child: Card(
              elevation: 5,
              color: Colors.grey[100],
              child: Container(
                padding: EdgeInsets.only(
                  left: 10,
                  right: 10,
                  bottom: 15,
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Image.network(ulasan.photo, width: 35, height: 35,fit: BoxFit.cover,),
                        ),
                        Container(width: 5,),
                        Container(
                          padding: EdgeInsets.only(
                            top: 10,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: MediaQuery.of(context).size.width - 98,
                                child: Text(
                                ulasan.title,
                                style: TextStyle(
                                  fontSize: 16,
                                ),
                              ),),
                              Row(
                                children: <Widget>[
                                  Icon(Mdi.star, color: Colors.orange, size: 14,),
                                  Icon(Mdi.star, color: Colors.orange, size: 14,),
                                  Icon(Mdi.star, color: Colors.orange, size: 14,),
                                  Icon(Mdi.star, color: Colors.orange, size: 14,),
                                  Icon(Mdi.star, color: Colors.orange, size: 14,),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: 10,
                      ),
                      child: Text(
                        "Esse minim esse excepteur ad mollit dolore. Ad reprehenderit consequat irure tempor voluptate enim consectetur velit cupidatat irure ex minim sit. Cupidatat cillum nostrud aute excepteur dolor nulla aliqua do quis. Eiusmod fugiat eiusmod sunt eiusmod voluptate dolore consequat fugiat commodo Lorem amet.",
                      ),
                    ),
                  ],
                ),

              ),
            ),
          ),
        ),
      );
    }

    setState(() {
      
    });
    
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: ulasanWidgetList,
      ),
    );
  }
}