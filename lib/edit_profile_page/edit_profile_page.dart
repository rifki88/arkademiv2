import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class EditProfilePage extends StatefulWidget {
  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            headerWidget(),
            listTileBlue(
              "assets/user-edit.svg",
              "EDIT PROFIL"
            ),
            Container(
              padding: EdgeInsets.only(
                top: 20,
                left: 15,
                right: 15,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  
                  Text(
                    "Nama",
                    style: TextStyle(
                      fontSize: 11,
                    ),
                  ),
                  TextFormField(
                    
                  ),
                  Container(height: 30,),
                  Text(
                    "Lokasi",
                    style: TextStyle(
                      fontSize: 11,
                    ),
                  ),
                  TextFormField(
                    
                  ),
                  Container(height: 30,),
                  Text(
                    "Bio",
                    style: TextStyle(
                      fontSize: 11,
                    ),
                  ),
                  TextFormField(
                    
                  ),

                  Container(height: 30,),
                  SizedBox(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(5.0)
                      ),
                      elevation: 5,
                      color: Color.fromRGBO(30, 130, 15, 1),
                      onPressed: _save,
                      child: Text(
                        "SIMPAN",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  Container(height: 30,),
                  
                ],
              ),
            )
          ]
        ),
      ),
    );
  }

  void _save() {

  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("PROFIL", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Image.asset("assets/avatar.png", width: 150,),
                        Text(
                          "Hilman Fajrian",
                          style: TextStyle(
                            fontSize: 20,
                          ),
                        ),
                        Text(
                          "HilmanFajrian@gmail.com",
                          style: TextStyle(
                            fontSize: 11,
                          ),
                        ),
                        Container(height: 30,),
                        
                      ],
                    ),
                  ],
                ),
              ),
              IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.chevron_left, color: Colors.white, size: 34,),
              ),
            ],
          )
        ),
      ],
    );
  }

  Widget listTileBlue(String svg, String title) {
    return InkWell(
      child: Container(
        color: Color.fromRGBO(239, 247, 254, 1),
        margin: EdgeInsets.only(
          bottom: 3,
        ),
        padding: EdgeInsets.only(
          top: 3,
          bottom: 3,
          left: 15,
          right: 5,
        ),
        child: ListTile(
          leading: SvgPicture.asset(
            svg,
            height: 25,
            color: Colors.black,
          ),
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20
            ),
          ),
        ),
      ),
    );
  }
}