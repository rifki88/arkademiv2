import 'package:arkademi/main_page/FollowerNotchedShape.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
class KuisQuestionPage extends StatefulWidget {
  @override
  _KuisQuestionPageState createState() => _KuisQuestionPageState();
}

class _KuisQuestionPageState extends State<KuisQuestionPage> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            headerWidget(),
            Container(
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 20,
                bottom: 20
              ),
              padding: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: Column(
                children: <Widget>[
                  InkWell(
                    child: Card(
                      color: currentIndex == 0 ? Color.fromRGBO(46, 139, 164, 1) : Color.fromRGBO(251, 251, 251, 1),
                      elevation: 5,
                      child: ListTile(
                        title: Text(
                          "lorem lorem",
                          style: TextStyle(
                            color: currentIndex == 0 ? Colors.white : Colors.black,
                          ),
                        ),
                        trailing: Icon(Icons.blur_circular),
                      )
                    ),
                  ),
                  InkWell(
                    child: Card(
                      elevation: 5,
                      child: ListTile(
                        title: Text("lorem lorem"),
                        trailing: Icon(Icons.blur_circular),
                      )
                    ),
                  ),
                  InkWell(
                    child: Card(
                      elevation: 5,
                      child: ListTile(
                        title: Text("lorem lorem"),
                        trailing: Icon(Icons.blur_circular),
                      )
                    ),
                  ),
                  InkWell(
                    child: Card(
                      elevation: 5,
                      child: ListTile(
                        title: Text("lorem lorem"),
                        trailing: Icon(Icons.blur_circular),
                      )
                    ),
                  ),
                  
                ],
              ),
            ),

          ],
        ),
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: (){
          
        },
        tooltip: 'Increment',
        backgroundColor: Colors.transparent,
        elevation: 0,
        child: SvgPicture.asset("assets/jempol.svg", height: 50,),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: FollowerNotchedShape(inverted: true),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            
            InkWell(
              onTap: () {
                
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Text(
                      "09 : 44",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color.fromRGBO(120, 120, 120, 1)
                      ),
                    ),
                    Text(
                      "Waktu Tersisa",
                      style: TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            
            InkWell(
              onTap: () {
                
              },
              child:Container(
                height: 50,
                width: 80,
                // color: Colors.red,
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: Text("Kumpulkan", textAlign: TextAlign.center,),
              ),
            ),

            
            InkWell(
              onTap: () {

              },
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(100)
                  )
                ),
                height: 60,
                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 2 - 40,
                child: Column(
                  children: <Widget>[
                    Text(
                      "10",
                      style: TextStyle(
                        fontSize: 20,
                        color: Color.fromRGBO(120, 120, 120, 1)
                      ),
                    ),
                    Text(
                      "Jumlah soal",
                      style: TextStyle(
                        fontSize: 12,
                        color: Color.fromRGBO(120, 120, 120, 1),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            
            
          ],
        ),
      )
    );
  }

  Widget headerWidget() {
    return Stack(
      children: <Widget>[
        Image.asset('assets/profile-header.png'),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          child: Stack(
            children: <Widget>[
              
              Container(
                margin: EdgeInsets.only(
                  top: 12
                ),
                width: MediaQuery.of(context).size.width,
                child: Text("KUIS", style: TextStyle(
                  fontSize: 18,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 65
                ),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Mendirikan Startup & Cara Membagi Saham",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        top: 10,
                      ),
                      child: Text(
                        "Kuis Mendirikan Startup",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 24,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(height: 15,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            SvgPicture.asset("assets/circle-number.svg", height: 70,),
                            Positioned(
                              top: 16,
                              left: 0,
                              right: 0,
                              child: Text(
                                "1",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 26,
                                  fontWeight: FontWeight.bold,
                                )
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              
            ]
          ),
        ),
      ],
    );
  }

}