import 'package:arkademi/auth_page/login_page.dart';
import 'package:arkademi/auth_page/register_page.dart';
import 'package:flutter/material.dart';

class AuthPage extends StatefulWidget {
  @override
  _AuthPageState createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  int _selectedIndex = 0;
  List<Widget> pages = [
    LoginPage(),
    RegisterPage(),
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Color.fromRGBO(43, 157, 153, 1),
        
          child: Stack(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height - 50,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/login-background.jpg")
                  )
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: 30,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Expanded(child: Container(),),
                        Image.asset("assets/login.png", width: 210, height: 210,),
                        Expanded(child: Container(),),
                      ],
                    ),
                    Container(
                      width: 280,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Color.fromRGBO(255, 255, 255, 0.5),
                        borderRadius: BorderRadius.circular(50),

                      ),
                      padding: EdgeInsets.fromLTRB(5,5,5,5),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              setState(() {
                                _selectedIndex = 0;
                              });
                            },
                            child: Container(
                              width: 130,
                              height: 30,
                              decoration: BoxDecoration(
                                color: _selectedIndex == 0 ? Colors.white : Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                              ),
                              
                              child: Center(child: Text("Masuk", style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: _selectedIndex == 0 ? Colors.black : Colors.white,
                              ),)),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              setState(() {
                                _selectedIndex = 1;
                              });
                            },
                            child: Container(
                              width: 130,
                              height: 30,
                              decoration: BoxDecoration(
                                color: _selectedIndex == 1 ? Colors.white : Colors.transparent,
                                borderRadius: BorderRadius.circular(50),
                              ),
                              
                              child: Center(child: Text("Daftar", style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: _selectedIndex == 1 ? Colors.black : Colors.white,
                              ),)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    PageStorage(
                      child: pages[_selectedIndex],
                      bucket: bucket,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}