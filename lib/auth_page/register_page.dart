import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 20 ,
          ),
          child: Text("Daftar dengan akun FB/Google", style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(25, 39, 130, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.facebook, color: Colors.white, size: 40,),
              ),
            ),
            FlatButton(
              onPressed: (){},
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(177, 6, 6, 1),
                  borderRadius: BorderRadius.circular(100)
                ),
                padding: EdgeInsets.fromLTRB(10, 8, 10, 12),
                child: Icon(Mdi.google, color: Colors.white, size: 40,),
              ),
            ),
            
          ],
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 10 ,
          ),
          child: Text("Atau mendaftar dengan alamat email", style: TextStyle(
            color: Colors.black,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),),
        ),
        Card(
          margin: EdgeInsets.only(
            top: 10,
            bottom: 60
          ),
          color: Colors.white,
          elevation: 5,
          child: Container(
            
            padding: EdgeInsets.all(20),
            decoration: BoxDecoration(
              
            ),
            width: 280,
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.accountCircleOutline),
                    hintText: "Username",

                  ),
                  keyboardType: TextInputType.text,
                ),
                Container(height: 10,),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.accountCardDetailsOutline),
                    hintText: "Nama",
                  ),
                  keyboardType: TextInputType.text,
                ),
                Container(height: 10,),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.email),
                    hintText: "Email",

                  ),
                  keyboardType: TextInputType.emailAddress,
                ),
                Container(height: 10,),
                TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Mdi.lock),
                    hintText: "Password",
                    suffixIcon: IconButton(
                      onPressed: () {},
                      icon: Icon(Mdi.eye),
                    )
                  ),
                  obscureText: true,
                  keyboardType: TextInputType.text,
                ),
                Container(height: 30,),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7)
                    ),
                    onPressed: (){},
                    color: Color.fromRGBO(255, 90, 0, 1),
                    child: Text("DAFTAR", style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),),
                  ),
                ),
                
                
              ],
            ),
          ),
        ),
        
        
      ],
    );
  }
}