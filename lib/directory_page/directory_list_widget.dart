import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';

class DirectoryListWidget extends StatefulWidget {
  @override
  _DirectoryListWidgetState createState() => _DirectoryListWidgetState();
}

class _DirectoryListWidgetState extends State<DirectoryListWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        for(int i = 0; i < 4; i++) card(),

      ],
    );
  }

  Widget card() {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => KelasPage()),
        ),
      child: Container(
        margin: EdgeInsets.only(
          bottom: 8,
          left: 15,
          right: 15
        ),
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 121,
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)
            ),
            color: Colors.grey[200],
            child: InkWell(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      ClipRRect(
                        borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(50),
                          topLeft: Radius.circular(8),
                        ),
                        child: Image.asset("assets/blog.jpg",
                          height: 80,
                          width: 130,
                          fit: BoxFit.cover,
                        ),
                      ),
                      
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 15,
                            right: 15
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(height: 10,),
                              Text("Mendirikan startup dan Cara membagi Saham",style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold
                              ),
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                              ),
                              Text("Mentor: HILMAN", style: TextStyle(
                                fontSize: 12,
                              ),
                              textAlign: TextAlign.left,
                              )
                            ],
                          )
                        ),
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10),
                      ),
                    color: Colors.blue,
                    ),
                    child: Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(50),
                              bottomLeft: Radius.circular(10)
                            ),
                            color: Colors.yellow
                          ),
                          height: 32,
                          width: 55,
                          child: Icon(Icons.group),
                        ),

                        Expanded(
                          child: Container(
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Text("130 siswa", style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.white
                                  ),),
                                ),
                                Row(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(width: 5,),
                                        Icon(Mdi.star, color: Colors.orange, size: 10,),
                                        Icon(Mdi.star, color: Colors.orange, size: 10,),
                                        Icon(Mdi.star, color: Colors.orange, size: 10,),
                                        Icon(Mdi.star, color: Colors.orange, size: 10,),
                                        Icon(Mdi.star, color: Colors.orange, size: 10,),
                                        Text("(4,5)", style: TextStyle(
                                          fontSize: 8,
                                          color: Colors.white,
                                        ),),
                                        Container(width: 5,),
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.yellow,
                          width: 130,
                          height: 32,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Rp 99.000", style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                              ),),
                              Container(width: 2,),
                              Text("Rp 500.000", style: TextStyle(
                                fontSize: 8,
                                fontWeight: FontWeight.bold,
                                color: Colors.red[700]
                              ),),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ),
          ),
        ),
      ),
    );
  }
}