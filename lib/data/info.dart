List<Info> infoList = [
  Info(
    id: 1,
    icon: 'http://client.gardana.id/arkademi/drafting-compass.png',
    label: "Diagnosa Usaha Rintisan"
  ),
  Info(
    id: 2,
    icon: 'http://client.gardana.id/arkademi/drafting-compass.png',
    label: "Tes Teknik Kewirausahaan"
  ),
  
];


class Info {
  final int id;
  final String icon;
  final String label;

  Info({
    this.id, 
    this.icon, 
    this.label,
  });
}