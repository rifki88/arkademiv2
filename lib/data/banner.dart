List<Banner> bannerList = [
  Banner(
    id: 1,
    image: 'http://client.gardana.id/arkademi/banner-diskon.jpg',
  ),
  Banner(
    id: 2,
    image: 'http://client.gardana.id/arkademi/banner-pajak.jpg',
  ),
  
];


class Banner {
  final int id;
  final String image;

  Banner({
    this.id, 
    this.image, 
  });
}