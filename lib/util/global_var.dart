import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

String numberFormat(int number) {
  if (number == null) number = 0;
  var numberFormat = new NumberFormat("###,###","id_ID");
  return numberFormat.format(number);
}