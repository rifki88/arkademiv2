import 'package:arkademi/kelas_page/kelas_page.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:arkademi/data/product.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

class ProductWidget extends StatefulWidget {
  final String title;
  final String subtitle;
  final VoidCallback viewAllButton;
  final List<Product> list;
  final bool showStar;

  ProductWidget({
    this.title,
    this.subtitle,
    this.viewAllButton,
    this.list,
    this.showStar,
  });
  @override
  _ProductWidgetState createState() => _ProductWidgetState(
    title: title,
    subtitle: subtitle,
    viewAllButton: viewAllButton,
    list: list,
    showStar: showStar,
  );
}

class _ProductWidgetState extends State<ProductWidget> {
  String title;
  String subtitle;
  VoidCallback viewAllButton;
  List<Product> list;
  bool showStar = false;

  _ProductWidgetState({
    this.title,
    this.subtitle,
    this.viewAllButton,
    this.list,
    this.showStar,
  });

 


 List dataJSON;
  
  Future<String>  ambildata() async {

  http.Response hasil = await http.get(
    Uri.encodeFull("https://staging-arkademi.kinsta.cloud/wp-json/wplms/v1/course"), headers: {
      "Accept":"application/json"
    }
  );  

  this.setState((){
    dataJSON = jsonDecode(hasil.body);
  }); 
  }

  @override
  void initState() {
    this.ambildata();
  }

  @override
  Widget build(BuildContext context) {
     return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => KelasPage()),
        );
      },
      
      child: Container(
        margin: EdgeInsets.only(
          top: 10,
          bottom: 0,
        ),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                left: 20,
                right: 20,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(title, style: TextStyle(
                                fontSize: 18,
                                color: Colors.grey[900]
                              ),),
                              showStar == true ? Row(
                                children: <Widget>[
                                  Container(width: 5,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                  Icon(Mdi.star, color: Colors.orange, size: 10,),
                                ],
                              ) : Container(),
                            ],
                          ),
                        ],
                      ),
                      Text(subtitle, style: TextStyle(
                        fontSize: 12,
                        color: Colors.grey[700],
                      ),),
                    ],
                  ),
                  InkWell(
                    onTap: () {

                    },
                    child: Icon(Mdi.menu, color: Colors.grey[400],size: 30,),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 12,
              ),
              height: 200,
              
              child: ListView.builder(
                itemCount: list.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                
                      
                  return buildItem(

                    index: index,
                    imageUrl: dataJSON[index]['data']['featured_image'],
                    title: dataJSON[index]['data']['name'],
                    rating: list[index].rating,
                    student: list[index].student,
                    price: list[index].price,
                    priceAfterDisc: list[index].priceAfterDisc,
                  );
                },
                
              ),

            )
          ],
        ),
      ),
    );
  }
}

class buildItem extends StatefulWidget {
  final int index;
  final String imageUrl;
  final String title;
  final double rating;
  final int student;
  final int price;
  final int priceAfterDisc;

  buildItem({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });
  @override
  _buildItemState createState() => _buildItemState(
    index: index,
    title: title,
    rating: rating,
    student: student,
    imageUrl: imageUrl,
    price: price,
    priceAfterDisc: priceAfterDisc,
  );
}

class _buildItemState extends State<buildItem> {
  int index;
  String imageUrl;
  String title;
  double rating;
  int student;
  int price;
  int priceAfterDisc;

  _buildItemState({
    this.index,
    this.imageUrl,
    this.title,
    this.rating,
    this.student,
    this.price,
    this.priceAfterDisc,
  });


  


  @override
  Widget build(BuildContext context) {
    return Container(
      width: 175,
      margin: EdgeInsets.only(
        left: index == 0 ? 16 : 5,
        right: index == 2 ? 16 : 0,
      ),
      child: Stack(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 180,
            child: Card(
              color: Color.fromRGBO(241, 241, 241, 1),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8)
              ),
              child: Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                    ),
                    child: Image.network(imageUrl),
                  ),
                  Container(
                    height: 84,
                    padding: EdgeInsets.only(
                      left: 5,
                      right: 5,
                      top: 5,
                    ),
                    child: Column(
                      children: <Widget>[
                        Text(title, 
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          color: Colors.black
                        ),),
                        Container(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Icon(Mdi.star, color: Colors.orange, size: 10,),
                                Text("($rating)", style: TextStyle(
                                  fontSize: 10,
                                ),),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Mdi.accountGroup, size: 10, color: Colors.black,),
                                Text("$student siswa", style: TextStyle(
                                  fontSize: 10
                                ),)
                              ],
                            )
                          ],
                        ),

                      ],
                    ),
                  ),

                  
                ],
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: 160,
            ),
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 25,
              child: FlatButton(
                color: Color.fromRGBO(9, 120, 191, 1),
                onPressed: () {
                  
                },
                child: _priceWidget(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _priceWidget() {
    if (price == 0 && priceAfterDisc == null) {
      return Text("GRATIS", style: TextStyle(
                fontSize: 16,
                color: Colors.white,
              ),);
    } else if(price > 0 && priceAfterDisc == null) {
      return Text(price.toString(), style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),);
    } else {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(priceAfterDisc.toString(), style: TextStyle(
                  fontSize: 16,
                  color: Colors.white,
                ),),
                Text(price.toString(), style: TextStyle(
                  fontSize: 11,
                  decoration: TextDecoration.lineThrough,
                  color: Colors.white,
                ),),
              ],
            );
    }
  }
}