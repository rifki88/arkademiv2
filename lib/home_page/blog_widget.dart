import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'package:arkademi/kelas_page/kelas_page.dart';

class BlogWidget extends StatefulWidget {
  @override
  _BlogWidgetState createState() => _BlogWidgetState();
}

class _BlogWidgetState extends State<BlogWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: 20,
        bottom: 0,
      ),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(
              left: 20,
              right: 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Blog", style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey[900]
                    ),),
                    Text("Bacaan bergizi kewirausahaan khas Arkademi", style: TextStyle(
                      fontSize: 12,
                      color: Colors.grey[700],
                    ),),
                  ],
                ),
                InkWell(
                  onTap: () {

                      Navigator.push(   context,
                        MaterialPageRoute(builder: (context) => KelasPage()), 
                      );


                  },
                  child: Icon(Mdi.menu),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
            padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
            child: Column(
              children: <Widget>[
                for(int i = 1; i <= 3; i++) blogItem()
              ],
            ),
          ),
          
        ]
      ),
    );
  }

  Widget blogItem() {
    return Container(
      margin: EdgeInsets.only(
        bottom: 8,
      ),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 90,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8)
          ),
          color: Colors.grey[200],
          child: InkWell(

            onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => KelasPage()),
                );
              },


            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(50),
                    topLeft: Radius.circular(8),
                    bottomLeft: Radius.circular(8),
                  ),
                  child: Image.asset("assets/blog.jpg",
                    height: 90,
                    width: 110,
                    fit: BoxFit.cover,
                  ),
                ),
                
                Expanded(
                  child: Container(
                    margin: EdgeInsets.only(
                      left: 15,
                      right: 15
                    ),
                    child: Text("Mengapa anda takan menang Melawan Gojek",style: TextStyle(
                      fontSize: 14,

                    ),
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
              ],
            )
          ),
        ),
      ),
    );
  }
}