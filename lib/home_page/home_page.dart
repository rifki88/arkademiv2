import 'package:arkademi/home_page/info_widget.dart';
import 'package:flutter/material.dart';
import 'package:mdi/mdi.dart';
import 'blog_widget.dart';
import 'category_widget.dart';
import 'header_widget.dart';
import 'product_widget.dart';
import 'package:arkademi/data/product.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              HeaderWidget(),
              ProductWidget(
                title: "Kelas & Tool Gratis",
                subtitle: "Perjalanan anda dimulai dari sini",
                list: productList,
                viewAllButton: () {
                },
              ),
              InfoWidget(),
              ProductWidget(
                title: "Kelas populer",
                subtitle: "Kelas paling diminati dan jumlah siswa terbanyak",
                list: productList2,
                showStar: true,
                viewAllButton: () {
                  
                },
              ),
              CategoryWidget(),
              BlogWidget(),
              Container(
                margin: EdgeInsets.only(
                  top: 15,
                ),
                padding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                child: Column(
                  children: <Widget>[
                    Divider(height: 3,),
                    Container(height: 20,),
                    Text("ARKADEMI V2.1", style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),),
                    Container(height: 5,),
                    Text("All rights reserved PT Arkademi Daya Indonesia 2017-2019", style: TextStyle(
                      fontSize: 10,
                    ),),
                    FlatButton.icon(
                      onPressed: () {

                      },
                      icon: Icon(Mdi.email, color: Colors.grey[600], size: 16,),
                      label: Text("KONTAK", style: TextStyle(
                        color: Colors.grey[600],
                        fontSize: 14,
                      ),),
                    ),
                    Container(height: 50,),
                  ],
                )
              ),
              
            ],
          ),
        ),
      ),
    );
  }

}